// urls
export const SERVER_BASE_URL = "http://localhost:3001";
export const SERVER_STATIC_IMAGES_URL = `${SERVER_BASE_URL}/images`;
export const SERVER_PUBLIC_IMAGES_API_URL = `${SERVER_BASE_URL}/api/public/images`;
export const SERVER_GET_IMAGE_IDS_API = `${SERVER_PUBLIC_IMAGES_API_URL}/image-ids`;
export const SERVER_GET_ALL_REACTIONS = `${SERVER_PUBLIC_IMAGES_API_URL}/reactions`;
export const SERVER_ADD_REACTION = `${SERVER_PUBLIC_IMAGES_API_URL}/add-reaction`;
