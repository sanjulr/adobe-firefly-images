import { Row, Skeleton } from "antd";
import superagent from "superagent";
import { SERVER_GET_ALL_REACTIONS } from "../config";
import ImageController from "../components/ImageController";
import { useEffect, useState } from "react";

const Images = () => {
  const [reactions, setReactions] = useState();
  const loadImagesWithReactions = async () => {
    const response = await superagent.get(SERVER_GET_ALL_REACTIONS);
    setReactions(response.body);
  };

  useEffect(() => {
    loadImagesWithReactions();
  }, []);

  if (reactions) {
    const images = Object.keys(reactions).map((imageId) => (
      <span key={imageId} style={{ flex: "33%" }}>
        <ImageController
          imageId={parseInt(imageId)}
          reactionsCount={reactions[imageId]}
        />
      </span>
    ));
    return <Row>{images}</Row>;
  }
  return <Skeleton />;
};
export default Images;
