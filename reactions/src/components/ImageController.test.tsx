import { act, render, screen } from "@testing-library/react";
import ImageController from "./ImageController";
import ReactionButton from "./ReactionButton";
import userEvent from "@testing-library/user-event";
import superagent from "superagent";

// setup is available only from v14. Referred from https://stackoverflow.com/a/72195119
const user = userEvent.setup();

const setupImageController = () => {
  render(
    <ImageController imageId={100} reactionsCount={{ up: 50, down: 20 }} />
  );

  const image = screen.getByRole("img");
  const upButton = screen.getByRole("button", { name: "+" });
  const downButton = screen.getByRole("button", { name: "-" });

  return { image, upButton, downButton };
};

const setupReactionButton = () => {
  render(<ReactionButton imageId={100} action={"up"} count={30} />);

  const button = screen.getByRole("button");
  const count = screen.getByLabelText("count");

  return { count, button };
};

describe("ImageController component", () => {
  it("renders the image with up and down vote buttons", () => {
    const { image, upButton, downButton } = setupImageController();
    expect(image).toBeVisible();
    expect(upButton).toBeVisible();
    expect(downButton).toBeVisible();
  });

  describe("ReactionButton component", () => {
    it("renders the button with count", () => {
      const { button, count } = setupReactionButton();
      expect(button).toBeVisible();
      expect(count).toBeVisible();
      expect(count).toHaveTextContent("30");
    });

    it("increases the count by one for the action clicked", async () => {
      const { button, count } = setupReactionButton();
      // @ts-ignore
      jest.spyOn(superagent, "post").mockReturnValue({
        send: jest.fn().mockReturnValue({ body: { up: 31, down: 20 } }),
      });
      await act(async () => {
        await user.click(button);
      });
      expect(count).toHaveTextContent("31");
    });
  });
});
