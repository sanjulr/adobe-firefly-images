import { SERVER_STATIC_IMAGES_URL } from "../config";
import { Card } from "antd";
import ReactionButton from "./ReactionButton";

export interface ReactionsCount {
  up: number;
  down: number;
}

export interface Props {
  imageId: number;
  reactionsCount: ReactionsCount;
}

const ImageController = ({ imageId, reactionsCount }: Props) => {
  return (
    <Card
      style={{ padding: 10, margin: 10 }}
      bordered
      cover={
        <img
          alt={imageId.toString()}
          src={`${SERVER_STATIC_IMAGES_URL}/${imageId}.png`}
          style={{ width: 300, height: 300 }}
        />
      }
      actions={[
        <ReactionButton
          imageId={imageId}
          action="up"
          count={reactionsCount.up}
        />,
        <ReactionButton
          imageId={imageId}
          action="down"
          count={reactionsCount.down}
        />,
      ]}
    ></Card>
  );
};

export default ImageController;
