import { Button, Space, notification } from "antd";
import superagent from "superagent";
import { SERVER_ADD_REACTION } from "../config";
import { useState } from "react";

export type Action = "up" | "down";

export interface Props {
  imageId: number;
  action: Action;
  count: number;
}

const ReactionButton = ({ imageId, action, count }: Props) => {
  const [reactionCount, setReactionCount] = useState(count);

  const text = action === "up" ? "+" : "-";

  const addReaction = async () => {
    try {
      const response = await superagent
        .post(SERVER_ADD_REACTION)
        .send({ imageId, reaction: action });
      setReactionCount(response.body[action]);
    } catch (err) {
      notification.error({ message: "Failed to update count" });
    }
  };

  return (
    <Space direction="horizontal">
      <Button onClick={addReaction}>{text}</Button>
      <span aria-label="count">{reactionCount}</span>
    </Space>
  );
};

export default ReactionButton;
