import "./App.css";
import Images from "./pages/Images";

const App = () => {
  return (
    <div>
      <Images />
    </div>
  );
};

export default App;
