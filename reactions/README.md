## About

Developed by Sanjeevi Lingam Rajasekaran (sanju4lr@gmail.com)

## Prerequisites

1. Node JS installed (this app was built on v20.10.0)

## Docker Setup [Recommended]

1. Install docker.
2. Build the docker images by running run `docker compose build` from the root folder.
3. From the root folder, run `docker compose up` to start the apps.
4. The frontend app is exposed to port 3000. So you can open the browser and goto http://localhost:3000/ to use the app.
5. The backend app is exposed to port 3001.

## Local Setup [Optional]

1. Install the backend app dependencies by running `npm --prefix reactions/server install`
2. Install the frontend app dependencies by running `npm --prefix reactions install`
3. Start the backend app by running `npm --prefix reactions/server start`
4. Start the frontend app by running `npm --prefix reactions start`

## Tests

1. Run the backend tests by running `npm --prefix reactions/server run test`
2. Run the frontend tests by running `npm --prefix reactions run test`

## Working on the app

1. The app will always display a static set of images served from the backend.
2. The app's backend storage is on memory only, meaning the memory will reset on backend app restart.
3. Use the `+` button for thumbs up action and `-` button for thumbs down action.
4. Supports simultaneous access and will reflect the correct values on the frontend.

## Codebase

1. The frontend app codebase is everything in the `/reactions` folder except the `/reactions/server` folder.
2. The backend app codebase is everything in the `/reactions/server` folder.
3. The frontend app is a React app bootstrapped using create-react-app and uses Typescript. Tests are run using jest.
4. The backend app is a Node JS app using Express JS server and uses Typescript. Tests are run using jest.
