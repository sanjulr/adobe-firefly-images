import {
  addReaction,
  getImageIds,
  getReactions,
} from "../../src/handlers/images";
import { ImageId, Reaction, _resetMemory } from "../../src/storage";

describe("Handlers", () => {
  beforeEach(() => {
    _resetMemory();
  });

  it("returns all the image ids", () => {
    expect(getImageIds()).toEqual(["1", "2", "3", "4", "5", "6"]);
  });

  it.each(["up", "down"])("should add a %s reaction to an image", (action) => {
    const reaction = action as Reaction;
    expect(getReactions()[1][reaction]).toEqual(0);
    const imageReactions = addReaction(1, reaction);
    expect(imageReactions[reaction]).toEqual(1);
    expect(getReactions()[1][reaction]).toEqual(1);
  });

  it("throws error for invalid image ids", () => {
    try {
      addReaction(12 as ImageId, "up");
      throw new Error("Unexpected");
    } catch (err) {
      expect((err as Error).message === "Invalid image id");
    }
  });

  it("throws error for invalid reaction", () => {
    try {
      addReaction(1, "thumbs up" as Reaction);
      throw new Error("Unexpected");
    } catch (err) {
      expect((err as Error).message === "Invalid reaction");
    }
  });

  it("get all reactions", () => {
    let allReactions = getReactions();
    Object.values(allReactions).forEach((reactions) => {
      expect(reactions.up).toEqual(0);
      expect(reactions.down).toEqual(0);
    });

    addReaction(1, "up");
    addReaction(3, "down");
    addReaction(3, "down");

    allReactions = getReactions();
    expect(allReactions[1].up === 1);
    expect(allReactions[3].down === 2);
  });
});
