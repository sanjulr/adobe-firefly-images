interface UpDowns {
  up: number;
  down: number;
}

export interface Memory {
  1: UpDowns;
  2: UpDowns;
  3: UpDowns;
  4: UpDowns;
  5: UpDowns;
  6: UpDowns;
}

// could use a database to permanently store this data
export let memory: Memory = {
  1: { up: 0, down: 0 },
  2: { up: 0, down: 0 },
  3: { up: 0, down: 0 },
  4: { up: 0, down: 0 },
  5: { up: 0, down: 0 },
  6: { up: 0, down: 0 },
};

export type Reaction = "up" | "down";
export type ImageId = keyof Memory;

export const addReaction = (imageId: ImageId, reaction: Reaction) => {
  if (reaction === "up") {
    memory[imageId].up++;
  } else if (reaction === "down") {
    memory[imageId].down++;
  }
  return memory[imageId];
};

export const getReactions = () => {
  return memory;
};

export const getImageIds = () => {
  return Object.keys(memory);
};

export const _resetMemory = () => {
  memory = {
    1: { up: 0, down: 0 },
    2: { up: 0, down: 0 },
    3: { up: 0, down: 0 },
    4: { up: 0, down: 0 },
    5: { up: 0, down: 0 },
    6: { up: 0, down: 0 },
  };
};

export default { addReaction, getReactions, getImageIds };
