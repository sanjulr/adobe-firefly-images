import express from "express";
import cors from "cors";
import router from "./api";
import path from "path";

const port = 3001;

const app = express();

// usage of express.json() referred from https://stackoverflow.com/a/10007542
app.use(express.json());
app.use(cors());

// usage of static referred from https://expressjs.com/en/starter/static-files.html
app.use("/images", express.static(path.join(__dirname, "images")));

app.use("/api", router);

app.listen(port, () => {
  console.log(`server running at port ${port}`);
});
