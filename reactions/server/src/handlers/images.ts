import storage, { ImageId, Reaction, memory } from "../storage";

/*
Not much to abstract here in this case but its a good practice to separate out the business logic away from the API handler
*/

export const getReactions = () => {
  return storage.getReactions();
};

export const addReaction = (imageId: ImageId, reaction: Reaction) => {
  if (memory[imageId]) {
    if (memory[imageId].hasOwnProperty(reaction)) {
      return storage.addReaction(imageId, reaction);
    } else {
      throw new Error("Invalid reaction");
    }
  } else {
    throw new Error("Invalid image id");
  }
};

export const getImageIds = () => {
  return storage.getImageIds();
};
