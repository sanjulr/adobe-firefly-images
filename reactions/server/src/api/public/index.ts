import { Router } from "express";
import imagesRouter from "./images";

const publicRouter = Router();

publicRouter.use("/images", imagesRouter);

publicRouter.get("/", (req, res) => {
  res.send("Welcome to image reactions API!");
});

export default publicRouter;
