import { Router } from "express";
import { ImageId, Reaction } from "../../storage";
import { getReactions, addReaction, getImageIds } from "../../handlers/images";

const imagesRouter = Router();

imagesRouter.get("/reactions", (req, res) => {
  try {
    res.json(getReactions());
  } catch (err) {
    res.status(500).send("Error retrieving reactions");
  }
});

imagesRouter.post("/add-reaction", (req, res) => {
  try {
    const { imageId, reaction }: { imageId: ImageId; reaction: Reaction } =
      req.body;
    res.json(addReaction(imageId, reaction));
  } catch (err) {
    if (err instanceof Error) {
      res.status(500).send(err.message);
    } else {
      console.error(err);
      res.status(500).send("Unknown error");
    }
  }
});

imagesRouter.get("/image-ids", (req, res) => {
  try {
    res.json({ imageIds: getImageIds() });
  } catch (err) {
    res.status(500).send("Error retrieving reactions");
  }
});

export default imagesRouter;
